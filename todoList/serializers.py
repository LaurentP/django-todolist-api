from rest_framework import serializers
from .models import Todo

# Create a serializer to allow complex data such as querysets and model instances
# to pass from Python datatype to JSON, XML or other...

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = '__all__'