from django.db import models

# Create your models here.
# Create or model with 3 attributes: todo_id / todo_title / todo_body
class Todo(models.Model):
    todo_id = models.AutoField(primary_key=True)
    todo_title = models.CharField(max_length=250)
    todo_body = models.TextField()
    